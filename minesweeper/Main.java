package ch.zbw.kobelt.minesweeper;

/**
 * Die Main Klasse erstellt das komplette Spiel beim Start der Applikation.
 * @author Philip Kobelt
 * @version 1.0, 16.03.2016
 */
public class Main {

	/**
	 * Die Main Methode erstellt ein neues Objekt des Spielfensters.
	 * @param args
	 */
	public static void main(String[] args) {
		new Spielfenster();
	}
}
